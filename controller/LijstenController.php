<?php

require(ROOT . "model/LijstenModel.php");

function index(){
	$lijsten = getAllLijsten();
	render('lijsten/index', $lijsten);
}

function createlijst(){
	$taken = getAllTaken();
	render('lijsten/createlijst', array("taken"=> $taken));
}

function store(){
	createNewLijst();
	header('Location: index');
}

function updateLijst($id){
	$updatelijst = getLijst($id);
	$taken = getAllTaken();
	render('lijsten/updatelijst', array("lijsten"=> $updatelijst, "taken"=> $taken));
}

function editlijst(){
	$id = $_POST["id"];
	updateALijst($id);
	header('Location: index');
}

function deletelijst($id){
	$deletelijst = getLijst($id);
	render('lijsten/deletelijst', array("lijsten"=> $deletelijst));
}

function destroylijst(){
	$id = $_POST["id"];
	deleteALijst($id);
	header('Location: index');
}