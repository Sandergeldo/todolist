<?php

require(ROOT . "model/TakenModel.php");

function index(){
	$taken = getAllTaken();
	render('taken/index', $taken);
}

function createtaak(){
	render('taken/createtaak');
}

function store(){
	createNewTaak();
	header('Location: index');
}

function updateTaak($id){
	$updatetaak = getTaak($id);
	render('taken/updatetaak', array("taken"=> $updatetaak));
}

function edittaak(){
	$id = $_POST["id"];
	updateATaak($id);
	header('Location: index');
}

function deletetaak($id){
	$deletetaak = getTaak($id);
	render('taken/deletetaak', array("taken"=> $deletetaak));
}

function destroytaak(){
	$id = $_POST["id"];
	deleteATaak($id);
	header('Location: index');
}