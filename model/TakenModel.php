<?php

function getAllTaken(){

	$db = openDatabaseConnection();

	$sql = "SELECT * FROM Taken";
	$query = $db->prepare($sql);
	$query->execute();

	$db = null;
	return $query->fetchAll();
}

function getTaak($id){
	$conn = openDatabaseConnection();
	$stmt = $conn->prepare("SELECT * FROM Taken WHERE id = :id");
	$stmt->bindParam(":id", $id);
	$stmt->execute();
	$result = $stmt->fetch();
	return $result;
}

function createNewTaak(){
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$naam = $_POST["naam"];
		$beschrijving = $_POST["beschrijving"];
		$status = $_POST["status"];
		$duur = $_POST["duur"];

try{
	$conn = openDatabaseConnection();
	$stmt = $conn->prepare("INSERT INTO Taken (naam, beschrijving, status, duur) VALUES (:naam,:beschrijving,:status,:duur)");
	$stmt->bindParam(":naam", $naam);
	$stmt->bindParam(":beschrijving", $beschrijving);
	$stmt->bindParam(":status", $status);
	$stmt->bindParam(":duur", $duur);	
	$stmt->execute();
}

catch(PDOException $e){

	echo "Connection failed: " . $e->getMessage();
}
	$conn = null;
}
}

function updateATaak($id){
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		$id = $_POST["id"];
		$naam = $_POST["naam"];
		$beschrijving = $_POST["beschrijving"];
		$status = $_POST["status"];
		$duur = $_POST["duur"];

	try{
		$conn = openDatabaseConnection();	
		$stmt = $conn->prepare("UPDATE Taken SET naam = :naam, beschrijving = :beschrijving, status = :status, duur = :duur WHERE id = :id");
		$stmt->bindParam(":id", $id);
		$stmt->bindParam(":naam", $naam);
		$stmt->bindParam(":beschrijving", $beschrijving);
		$stmt->bindParam(":status", $status);
		$stmt->bindParam(":duur", $duur);		
		$stmt->execute();
		header('Location: index');
	}
	catch(PDOException $e){
		echo "Connection failed: " . $e->getMessage();
	}
	$conn = null;
	}
}

function deleteATaak($id){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		try{
			$conn = openDatabaseConnection();
			$stmt = $conn->prepare("DELETE FROM Taken WHERE id = :id");
			$stmt->bindParam("id", $id);
			$stmt->execute();
		}
		catch(PDOException $e){
			echo "Connection failed: " . $e->getMessage();
		}
		$conn = null;
		}
	}
