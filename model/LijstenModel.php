<?php

function getAllLijsten(){

	$db = openDatabaseConnection();

	$sql = "SELECT * FROM Lijsten";
	$query = $db->prepare($sql);
	$query->execute();

	$db = null;
	return $query->fetchAll();
}

function getLijst($id){
	$conn = openDatabaseConnection();
	$stmt = $conn->prepare("SELECT * FROM Lijsten WHERE id = :id");
	$stmt->bindParam(":id", $id);
	$stmt->execute();
	$result = $stmt->fetch();
	return $result;
}

function createNewLijst(){
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$naam = $_POST["naam"];
		$taak_1 = $_POST["taak_1"];
		$taak_2 = $_POST["taak_2"];
		$taak_3 = $_POST["taak_3"];

try{
	$conn = openDatabaseConnection();
	$stmt = $conn->prepare("INSERT INTO Lijsten (naam, taak_1, taak_2, taak_3) VALUES (:naam,:taak_1,:taak_2,:taak_3)");
	$stmt->bindParam(":naam", $naam);
	$stmt->bindParam(":taak_1", $taak_1);
	$stmt->bindParam(":taak_2", $taak_2);
	$stmt->bindParam(":taak_3", $taak_3);
	$stmt->execute();
}

catch(PDOException $e){

	echo "Connection failed: " . $e->getMessage();
}
	$conn = null;
}
}

function getAllTaken(){

	$db = openDatabaseConnection();

	$sql = "SELECT * FROM Taken";
	$query = $db->prepare($sql);
	$query->execute();

	$db = null;
	return $query->fetchAll();
}

function updateALijst($id){
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		$id = $_POST["id"];
		$naam = $_POST["naam"];
		$taak_1 = $_POST["taak_1"];
		$taak_2 = $_POST["taak_2"];
		$taak_3 = $_POST["taak_3"];

	try{
		$conn = openDatabaseConnection();	
		$stmt = $conn->prepare("UPDATE Lijsten SET naam = :naam, taak_1 =:taak_1,  taak_2 = :taak_2, taak_3 = :taak_3 WHERE id = :id");
		$stmt->bindParam(":id", $id);
		$stmt->bindParam(":naam", $naam);
		$stmt->bindParam(":taak_1", $taak_1);
		$stmt->bindParam(":taak_2", $taak_2);
		$stmt->bindParam(":taak_3", $taak_3);		
		$stmt->execute();
		header('Location: index');
	}
	catch(PDOException $e){
		echo "Connection failed: " . $e->getMessage();
	}
	$conn = null;
	}
}

function deleteALijst($id){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		try{
			$conn = openDatabaseConnection();
			$stmt = $conn->prepare("DELETE FROM Lijsten WHERE id = :id");
			$stmt->bindParam("id", $id);
			$stmt->execute();
		}
		catch(PDOException $e){
			echo "Connection failed: " . $e->getMessage();
		}
		$conn = null;
		}
	}

