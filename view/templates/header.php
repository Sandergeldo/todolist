<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>To-Do-List</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=URL?>/public/js/sorttaken.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=URL?>/public/css/taken.css">
</head>
<body>
	<div id="container" class="container">
		<div class="navbar">
			<nav>
				<ul>
					<li><a  href="<?=URL?>home/index">Home</a></li>
					<li><a  href="<?=URL?>lijsten/index">Lijsten</a></li>
					<li><a  href="<?=URL?>taken/index">Taken</a></li>
				</ul>
			</nav>
		</div>
		

